---
stages:
  - lint
  - clean
  - user
  - project
  - security_flavors_network
  - servers
  - pre_servers_config
  - servers_config

##
# Variables
##

variables:
  admin_openrc_path: vars/openstack_openrc
  GIT_SUBMODULE_STRATEGY: recursive
  CHAINED_CI_INIT: scripts/chained-ci-tools/chained-ci-init.sh

##
# Linting
##

.syntax_checking: &syntax_checking
  stage: lint
  extends: .syntax_checking_tags
  except:
    - schedules
    - triggers
    - web

yaml_checking:
  image: sdesbure/yamllint:latest
  script:
    - yamllint *.yml
    - yamllint inventory/group_vars/*.yml
  <<: *syntax_checking

ansible_linting:
  image: sdesbure/ansible-lint:latest
  script:
    - ansible-lint -x ANSIBLE0010,ANSIBLE0013 os_*.yml
  <<: *syntax_checking

##
# Generic
##

.ansible_run: &ansible_run
  extends: .ansible_run_tags
  image: ${ANSIBLE_DOCKER_IMAGE}:${ANSIBLE_DOCKER_TAG}
  only:
    - schedules
    - triggers
    - web

.chained_ci_tools_after: &chained_ci_tools_after
  retry: 2
  after_script:
    - ./scripts/chained-ci-tools/clean.sh

.chained_ci_tools_admin: &chained_ci_tools_admin
  <<: *chained_ci_tools_after
  before_script:
    - curl -s ifconfig.me || true
    - chmod 600 .
    - ./scripts/get_plugins.sh
    - . ./${CHAINED_CI_INIT} -a -i inventory/jumphost0_inventory
    - source <(cat_file vars/openstack_openrc)
    - >
      ansible-playbook ${ansible_verbose} \
        ${VAULT_OPT} \
        --extra-vars "local_cloud=NONE" \
        -i inventory/jumphost0_inventory os_cloud.yml

.chained_ci_tools_user: &chained_ci_tools_user
  <<: *chained_ci_tools_after
  before_script:
    - curl -s ifconfig.me || true
    - chmod 600 .
    - ./scripts/get_plugins.sh
    - . ./${CHAINED_CI_INIT} -a -i inventory/jumphost0_inventory
    - source <(cat_file vars/openstack_user_openrc)
    - >
      ansible-playbook ${ansible_verbose} \
        ${VAULT_OPT} \
        --extra-vars "local_cloud=NONE" \
        -i inventory/jumphost0_inventory os_cloud.yml

.chained_ci_tools_final: &chained_ci_tools_final
  <<: *chained_ci_tools_after
  before_script:
    - curl -s ifconfig.me || true
    - chmod 600 .
    - ./scripts/get_plugins.sh
    - . ./${CHAINED_CI_INIT} -a -i inventory/infra
    - source <(cat_file vars/openstack_user_openrc)
    - >
      ansible-playbook ${ansible_verbose} \
        ${VAULT_OPT} \
        --extra-vars "local_cloud=NONE" \
        -i inventory/infra os_cloud.yml

.only_trigger: &only_trigger
  only:
    refs:
      - triggers

.only_trigger_admin: &only_trigger_admin
  only:
    variables:
      - $ADMIN
    refs:
      - triggers

.only_trigger_not_admin: &only_trigger_not_admin
  only:
    refs:
      - triggers
  except:
    variables:
      - $ADMIN

.artifacts_to_admin: &artifacts_to_admin
  artifacts:
    paths:
      - inventory/jumphost0_inventory
      - inventory/host_vars/jumphost0.yml
      - vars/pdf.yml
      - vars/idf.yml
      - vars/user_cloud.yml
      - vars/openstack_user_openrc
      - vars/openstack_openrc
      - vars/ca.pem
      - vars/ca.crt
      - vars/vaulted_ssh_credentials.yml
      - vars/ssh_gateways.yml

.artifacts_to_user: &artifacts_to_user
  artifacts:
    paths:
      - inventory/jumphost0_inventory
      - inventory/host_vars/jumphost0.yml
      - vars/pdf.yml
      - vars/idf.yml
      - vars/user_cloud.yml
      - vars/project_infos.yml
      - vars/openstack_user_openrc
      - vars/ca.pem
      - vars/ca.crt
      - vars/vaulted_ssh_credentials.yml
      - vars/ssh_gateways.yml

.artifacts_final: &artifacts_final
  artifacts:
    paths:
      - inventory/infra
      - vars/idf.yml
      - vars/pdf.yml
      - vars/user_cloud.yml
      - vars/project_infos.yml
      - vars/openstack_user_openrc
      - vars/ca.pem
      - vars/ca.crt
      - vars/vaulted_ssh_credentials.yml
      - vars/ssh_gateways.yml

# clean when user (only known networks, servers and volumes)
user_clean:
  stage: clean
  <<: *ansible_run
  <<: *chained_ci_tools_user
  only:
    variables:
      - $CLEAN
    refs:
      - triggers
  except:
    variables:
      - $ADMIN
  script:
    - unset OS_INTERFACE
    - unset OS_TENANT_NAME
    - unset OS_USERNAME
    - unset OS_PROJECT_NAME
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_user_clean.yml

# clean when admin (tenant, user, ...)
admin_clean:
  stage: clean
  <<: *ansible_run
  <<: *chained_ci_tools_admin
  only:
    variables:
      - $CLEAN == $ADMIN
    refs:
      - triggers
  except:
    variables:
      - $ADMIN == ''
      - $ADMIN == null
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_admin_clean.yml
  retry: 2

# Create the cloud configs and the user
create_user:
  stage: user
  <<: *ansible_run
  <<: *artifacts_to_admin
  <<: *chained_ci_tools_after
  <<: *only_trigger
  script:
    - chmod 600 .
    - ./scripts/get_plugins.sh
    - . ./${CHAINED_CI_INIT} -a -i inventory/jumphost0_inventory
    - >  # If we run as admin we create the admin cloud.yaml then the user
      if [ -n "${ADMIN}" ]; then
          source <(cat_file vars/openstack_openrc)
          ansible-playbook ${ansible_verbose} \
            --extra-vars "local_cloud=NONE"  \
            ${VAULT_OPT} -i inventory/jumphost0_inventory os_cloud.yml
          ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
            -i inventory/jumphost0_inventory os_user.yml
      fi
    - source <(cat_file vars/openstack_user_openrc)
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_cloud.yml


create_project:
  stage: project
  <<: *ansible_run
  <<: *chained_ci_tools_admin
  <<: *artifacts_to_user
  <<: *only_trigger_admin
  script:
    - >
      ansible-vault decrypt ${VAULT_OPT} \
        vars/openstack_user_openrc || true
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_project.yml
    - source <(cat_file vars/openstack_user_openrc)
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_cloud.yml

create_flavors:
  stage: security_flavors_network
  <<: *ansible_run
  <<: *chained_ci_tools_admin
  <<: *only_trigger_admin
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_flavor.yml

.create_network: &create_network
  stage: security_flavors_network
  <<: *ansible_run
  <<: *chained_ci_tools_user
  <<: *artifacts_to_user
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_network.yml

create_network_user:
  <<: *create_network
  <<: *only_trigger_not_admin

create_network_admin:
  <<: *create_network
  <<: *only_trigger_admin
  dependencies:
    - create_project

create_security:
  stage: security_flavors_network
  <<: *ansible_run
  <<: *chained_ci_tools_user
  <<: *only_trigger
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_security.yml

create_servers:
  stage: servers
  <<: *ansible_run
  <<: *chained_ci_tools_user
  <<: *artifacts_final
  <<: *only_trigger
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/jumphost0_inventory \
        os_servers.yml
  dependencies:
    - create_network_admin
    - create_network_user

set_keys:
  stage: servers_config
  <<: *ansible_run
  <<: *chained_ci_tools_final
  <<: *artifacts_final
  <<: *only_trigger
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/infra \
        os_set_keys.yml
  dependencies:
    - create_servers

configure_disks:
  stage: pre_servers_config
  <<: *ansible_run
  <<: *chained_ci_tools_final
  <<: *artifacts_final
  <<: *only_trigger
  script:
    - ansible-galaxy install -r requirements.yml
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/infra \
        os_configure_disks.yml
  dependencies:
    - create_servers
  retry: 2

hardening_servers:
  stage: servers_config
  <<: *ansible_run
  <<: *chained_ci_tools_final
  <<: *artifacts_final
  only:
    variables:
      - $ENABLE_HARDENING
    refs:
      - triggers
  script:
    - ansible-galaxy install -r requirements.yml
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/infra \
        os_hardening.yml
  dependencies:
    - create_servers
  # hardening sometimes fails to install auditd the first time...
  retry: 2

configure_proxy:
  stage: servers_config
  <<: *ansible_run
  <<: *chained_ci_tools_final
  <<: *artifacts_final
  only:
    variables:
      - $VM_HTTP_PROXY
    refs:
      - triggers
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/infra \
        os_set_proxy.yml
  dependencies:
    - create_servers
