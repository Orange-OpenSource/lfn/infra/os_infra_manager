#!/usr/bin/env sh

# Get filter plugins from remote infra_manager_filters
#

export RUN_SCRIPT=${0}
export ROOT_FOLDER=${PWD}
export TOOLS_FOLDER=${ROOT_FOLDER}/scripts
. ${TOOLS_FOLDER}/chained-ci-tools/rc.sh

step_banner "Get infra_manager_filters"
IMF=https://gitlab.com/Orange-OpenSource/lfn/infra/infra_manager_filters/raw
IMF_BRANCH=master
FILTER_FOLDER=${ROOT_FOLDER}/filter_plugins

step_line "Get nodes.py"
wget -q -O ${FILTER_FOLDER}/nodes.py ${IMF}/${IMF_BRANCH}/src/nodes.py
step_line "Get size.py"
wget -q -O ${FILTER_FOLDER}/size.py ${IMF}/${IMF_BRANCH}/src/size.py
