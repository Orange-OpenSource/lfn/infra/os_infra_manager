---
##
# retrieve and configure kolla
#
- hosts: jumphost
  vars_files:
    - "vars/idf.yml"
    - "vars/pdf.yml"
  tasks:
    - name: ensure relevant pip packages are installed
      pip:
        name: "{{ python_packages }}"
        state: present
      become: true

    - name: get user list
      os_user_facts:
        cloud: "{{ cloud_admin }}"
        name: "{{ openstack_user_name }}"

    - name: Get list of clouds users
      os_client_config:
        clouds:
          - "{{ openstack_user_name }}"

    - name: get project infos
      os_project_facts:
        cloud: "{{ cloud_admin }}"
        name: "{{ openstack_tenant_name }}"

    - name: if user exists, delete all the tenant
      when: openstack_users|length > 0 and openstack_projects|length > 0
      block:
        - name: get servers list
          os_server_facts:
            cloud: "{{ openstack_user_name }}"
          register: check
          ignore_errors: true

        - name: "create the user {{ openstack_user_name }}"
          os_user:
            cloud: "{{ cloud_admin }}"
            state: present
            name: "{{ openstack_user_name }}"
            password: "__reset_by_admin_for_cleaning__"
            update_password: always
            domain: "{{ os_user_domain_name }}"
          when: check.failed

        - name: generate a new entry in clouds.yaml
          include_role:
            name: cloud
          vars:
            local_cloud: 'NONE'
            os_username: "{{ openstack_user_name }}"
            os_password: "__reset_by_admin_for_cleaning__"
            os_project_name: "{{ openstack_tenant_name }}"
            os_interface: "{{ openstack_user_interface }}"
          when: check.failed

        - name: get stacks
          shell: >
            openstack --os-cloud {{ openstack_user_name }} stack
            list --format json -c 'Stack Name'
          register: stack_list

        - name: delete stacks
          os_stack:
            cloud: "{{ openstack_user_name }}"
            name: "{{ item['Stack Name'] }}"
            state: absent
          loop:
            "{{ stack_list.stdout| default('[]', 1)|
                from_json }}"
          loop_control:
            label: "{{ item['Stack Name'] }}"

        - name: get servers list
          os_server_facts:
            cloud: "{{ openstack_user_name }}"

        - name: delete servers
          os_server:
            cloud: "{{ openstack_user_name }}"
            state: absent
            name: "{{ item.name }}"
            terminate_volume: true
            delete_fip: true
          loop: "{{ openstack_servers }}"
          loop_control:
            label: "{{ item.name }}"

        # no volume facts available
        - name: get volumes
          shell: >
            openstack --os-cloud {{ openstack_user_name }} volume
            list --format json -c Name
          register: volume_list
          ignore_errors: true

        - name: delete volumes
          os_volume:
            cloud: "{{ openstack_user_name }}"
            state: absent
            display_name: "{{ item.Name }}"
          loop:
            "{{ volume_list.failed |
                ternary('[]', volume_list.stdout| default('[]', 1))|
                from_json }}"
          loop_control:
            label: "{{ item.Name }}"
            pause: 1
          when: not volume_list.failed

        - name: get loadbalancer - bash way, no alternative in 2.7
          shell: >
            openstack --os-cloud {{ openstack_user_name }} loadbalancer
            list --format json -c id
          register: loadbalancer_list
          ignore_errors: true

        # os_loadbalancer ask for loadbalancer vip_network, vip_subnet or
        # vip_port but we don't care for deletion
        - name: delete loadbalancers - bash way, no alternative in 2.7
          shell: >
            openstack --os-cloud {{ openstack_user_name }} loadbalancer
            delete {{ item.id }} --cascade
          loop:
            "{{ loadbalancer_list.failed |
                ternary('[]', loadbalancer_list.stdout| default('[]', 1))|
                from_json }}"
          loop_control:
            label: "{{ item.id }}"
          when: not loadbalancer_list.failed
          ignore_errors: true

        - name: get routers - bash way, no alternative in 2.7
          shell: >
            openstack --os-cloud {{ openstack_user_name }} router
            list --format json -c Name
          register: router_list

        - name: delete routers
          os_router:
            cloud: "{{ openstack_user_name }}"
            state: absent
            name: "{{ item.Name }}"
          loop: "{{ router_list.stdout| default('[]', 1)| from_json }}"
          loop_control:
            label: "{{ item.Name }}"

        - name: get networks
          os_networks_facts:
            cloud: "{{ openstack_user_name }}"
            filters:
              "router:external": false
              "tenant_id": "{{ openstack_projects.0.id }}"

        # - name: retrieve remaining ports
        #   os_port_facts:
        #     cloud: "{{ openstack_user_name }}"
        #     filters:
        #       network_id: "{{ item.id }}"
        #   loop: "{{ openstack_networks }}"
        #   register: os_ports

        # - name: delete remaining ports
        #   os_port:
        #       cloud: "{{ openstack_user_name }}"
        #       network: "{{ item.network_id }}"
        #       mac_address: "{{ item.mac_address }}"
        #       name: "{{ item.name }}"
        #       state: absent
        #   loop:
        #     "{{ os_ports.results |
        #         map(attribute='ansible_facts.openstack_ports') |
        #         list | flatten }}"

        - name: delete networks
          os_network:
            cloud: "{{ openstack_user_name }}"
            state: absent
            name: "{{ item.name }}"
          loop: "{{ openstack_networks }}"
          loop_control:
            label: "{{ item.name }}"

        - name: get floating IPs
          shell: >
            openstack --os-cloud {{ openstack_user_name }} floating ip
            list -f json -c ID
          register: floating_list

        - debug:
            msg: "{{ floating_list }}"

        - name: delete floating IPs - bash way, no alternative in 2.7
          shell: >
            openstack --os-cloud {{ openstack_user_name }} floating ip
            delete {{ item.ID }}
          loop:
            "{{ floating_list.stdout| default('[]', 1)|
               from_json }}"
          loop_control:
            label: "{{ item.ID }}"

    - name: delete tenant
      os_project:
        cloud: "{{ cloud_admin }}"
        name: "{{ openstack_tenant_name }}"
        state: absent

    - name: delete user
      os_user:
        cloud: "{{ cloud_admin }}"
        name: "{{ openstack_user_name }}"
        state: absent
